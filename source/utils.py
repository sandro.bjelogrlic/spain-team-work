import numpy as np
import pandas as pd
import os.path

def calculate_best_features(data_in,method=None):
    """
    This function does nothing
    :param data_in: dataframe of data
    :param method: method to select features
    :return:
    """
    pass

def import_data(read_csv=True):
    """
    Function to import the data
    ---
    param:
        read_csv: (boolean) flag to read csv or other format. Other option is json
    returns:
        pandas dataframe containing the data
    """
    fname_csv = '../data/UCI_Credit_Card.csv'
    fname_json = '../data/UCI_Credit_Card.json'
    
    if read_csv:
        data = pd.read_csv(fname_csv)
  
    else:
        data = pd.read_json(fname_json)
    
    if not os.path.isfile(fname_csv) or os.path.isfile(fname_json):
        raise FileNotFoundError
     
    return data

def count_missings(df_in):
    """
        Returns a list with the name of the columns with missing values or the dataframe
        ---
        params:
            df_in: (dataframe) dataframe to be analysed
        returns:
            list of df_in columns with missing values
    """

    series=df_in.isnull().any()
    return series[series].index

def sex_to_category(var='SEX'):
    """
    Function to convert sex from numbers to categories.
    """
    if var == 1:
        return 'Male'
    elif var == 2:
        return 'Female'

def education_to_category(var='EDUCATION'):
    """
    Function to convert 'Education' into a cathegorical feature    
    ---
    arguments:
        var: (string) name of the column containing the education variable, default is 'EDUCATION'
        EDUCATION: (1=graduate school, 2=university, 3=high school, 4=others, 5=unknown, 6=unknown)  
    """
    if var == 1:
        return 'Graduate'
    elif var == 2:
        return 'University'
    elif var == 3:
        return 'High school'
    elif var == 4:
        return 'Others'
    else:
        return 'Unknown'

def convert_marriage(var='MARRIAGE'):
    """
    Creating a function which convers te marriage from numerical values to categories. 
    var = 'MARRIAGE' is defined outside the function
    It returns 1 for married, 2 for single & 3 for others  
    """
    if var == 1:
        return 'Married'
    elif var == 2:
        return 'Single'
    else:
        return 'Others'


dict_func = {'SEX': sex_to_category, 'MARRIAGE':convert_marriage, 'EDUCATION':education_to_category} #created a dictionary with variables and functions to apply to the data

def apply_function(data_in, to_convert):
    for k,v in dict_func.items():
        data_in[k]=data_in[k].apply(v)
    return data_in

def get_dummies(data_in, vrb = 'SEX'):
    """
    Function to get dummies for a given variable
    """
    dummies = pd.get_dummies(data[vrb])
    df = pd.concat((data_in, dummies), axis=1)
    return df

def get_sl(y):
    """
    Function to get the slope of a equaly spaced data set
    """
    x = np.array(range(len(y)))
    p0 = np.polyfit(x, y, 1)
    return p0[0]

def trend_payment(data_in):
    """
    Function to check if the slope is positive or negative: increasing or decreasing trend in the payment over the months
    """
    y = ['BILL_AMT1','BILL_AMT2','BILL_AMT3','BILL_AMT4','BILL_AMT5','BILL_AMT6']
    data_in['tot_amount'] = data_in[y].mean(axis=1) 
    data_in['Slope'] = data_in[y].apply(get_sl, axis=1)
    data_in['Normalized_Slope'] = data_in['Slope'] / data_in['tot_amount']
    return(data_in)

def hot_encoding_function (data, columnas = ['SEX','MARRIAGE']):
    """
    Function to convert specific cathegorical features into dummies
    ---
    arguments:
        data: input pandas dataframe
        columnas: cathegorical features to convert into dummies
    
    returns:
        pandas dataframe containing the updated data
        
    """
    data_out=pd.get_dummies(data, columns=columnas)
    
    return data_out

def test_is_dataframe(df):
    assert type(df)==pd.DataFrame


def bills_ratios (df_in):
    """
        Calculates the ratio between BILL_AMTn/LIMIT_BALn for all columns in df_in starting with 'BILL'
        ----
        params:
            df_in: (dataframe) input dataframe
        ----
        returns:
            a dataframe with ratios added
    """
    ratios=[col for col in df_in.columns if col[:4]=='BILL']

    for x in range(len(ratios)):
        df_in['BillLimit_Ratio_'+str(x+1)]=df_in['BILL_AMT'+str(x+1)]/df_in['LIMIT_BAL']
        
    return df_in

def apply_education_f (data_in):
    data_in['EDUCATION']=data_in['EDUCATION'].apply(education_to_category)
    return data_in



def split_train_test_samples (data, train_fraction=0.70):
    
    """
    Function to split a given dataframe into two different ones: train and test
    ---
    arguments:
        data: input pandas dataframe
        train_fraction: train fraction
    
    returns:
        Two pandas dataframe -train & test- with the same columns as the given dataframe
        
    """
    # Train dataframe is taken a given fraction from data dataframe     
    data_train = data.sample(frac=train_fraction) 
    # Test dataframe is collecting IDs of data dataframe not presence in data_train
    data_test = data[data.ID.apply(lambda x: x not in data_train.ID.values)]  
    
    return data_train, data_test

def traintest_check(df_train, df_test, train_fraction=0.7, margin=0.01):
    """
        Checks if df_train/(df_train+df_test) == train_fraction in terms of number of lines.
        We consider a margin in train_fraction of +-margin
        ----
        params:
            df_train: (dataframe) train sample
            df_test: (dataframe) test sample
            train_fraction: (float in [0,1]) size of train sample in relation with train+test size
            margin: (float) +-margin for sample to meet train_fraction parameter
        returns:
            True if the dataframes meet the proportion set in train_fraction taking into consideration the margin
        
    """
    assert round(abs((float(df_train.shape[0])/(df_train.shape[0] + df_test.shape[0])) - train_fraction),2) <= margin

def features_and_target(data_in, target):
    """
        Split the dataset into two Dataframes, one for features and another for target
        ----
        params:
            data_in: dataset
            target: 'default.payment.next.month'
        returns:
            A tuple of two DataFrames

    """
    features_df = data_in.drop(target, axis=1)
    target_series = data_in[target]
    return features_df, target_series

def drop_function(data, columns=['SEX', 'MARRIAGE']):
    """
    Function that accepts a list of columns to be dropped and returns a dataframe without them
    ---
    arguments:
        data: input pandas dataframe
        columns: features to be dropped

    returns:
        pandas dataframe containing the updated data
    """
    data2 = data.drop(axis=1, columns=columns)
    return data2


def correlation_with_target(df_features, df_targets, corr_threshold=0.6):
    """
        returns a list of columns in df_features that have a correlation with any column in df_targets higher than corr_threshold
        dataframes must have same index
        ----
        params:
            df_features: (dataframe) contains the features
            df_targets: (dataframe) contains the targets
            corr_threshold: (float in [0,1]) correlation threshold
        ----
        returns:
            list of df_features.columns with correlation with df_targets greater than corr_threshold
    
    """
    #Join features and targets by index
    joined_data= df_features.join(df_targets)
    
    #Calculate correlations matrix, keep only columns in df_targets and transpose results
    target_corrs=joined_data.corr()[[col for col in list(df_targets.columns)]].transpose()
    
    #Keep any column with correlation>corr_threshold with any column in df_targets
    target_corrs=target_corrs[abs(target_corrs)>corr_threshold]
    target_corrs=(~target_corrs.isnull()).any()
    
    #List them and return them if columns appear in df_features
    target_corrs=list(target_corrs[target_corrs==True].index)
    target_corrs=[v for v in target_corrs if v in list(df_features.columns)]

    return target_corrs
